# Meta-pipe user, authentication and session service

This is a small service designed to authorize Meta-pipe users and help authenticate them with the Elixir AAI.
The source code is provided as an illustration and will not be maintained in the future.

# Usage

**Note!** We have removed a keystore in the repository that is necessary to execute these tests. We will add the keystore when we get the permission to do so.

## Build and run

Build:

    mvn clean package -Dmaven.test.skip=true

Migrate a test database (optional depending on config)

    java -jar target/authservice-1.0-SNAPSHOT.jar db migrate src/test/resources/test-config.yml

Run the Authorization Server:

    java -jar target/authservice-1.0-SNAPSHOT.jar server src/test/resources/test-config.yml

## Obtain an OAuth2.0 access token (RFC-6749)

### Via Authorization Code Grant

1. Navigate your browser to http://localhost:8080/oauth2/authorize?response_type=code&client_id=system_web&redirect_uri=http://localhost:8080
2. Authenticate with the idp
3. When redirected back the URL in your borwser's address bar will look something like this: http://localhost:8080/?code=xxxxxxx
4. (in a terminal) Save the authorization code to environment and run the following cURL command:

~~~
export code=xxxxxxx
curl localhost:8080/oauth2/token -d "grant_type=authorization_code&code=$code&client_id=system_web&client_secret=xxxxxxx&redirect_uri=http://localhost:8080"
~~~

This should return an access token and a refresh token
    
    returns:
    {"access_token":"xxxxxxxx==","refresh_token":"xxxxx==","scope":"POST,GET,PUT|storage/yyyy POST|upload/api/frontend/request_upload GET,PUT|jobs/yyyy","token_type":"Bearer"}

### Via Client Credentials Grant

    curl localhost:8080/oauth2/token -d grant_type=client_credentials --user system_test-executor:yyyy

Or alternativley

    curl localhost:8080/oauth2/token -d 'client_id=system_test-executor&client_secret=yyyyy&grant_type=client_credentials'
    

## Inspect the token

(in a terminal) Save your access token to environment

    export access_token="xxxx=="
    


### Via token introspection (RFC-7662)

Run the following cURL command:

    curl localhost:8080/oauth2/introspect -d token=$access_token | jq .

This should return a token introspection response:

    {
      "scope": "POST,GET,PUT|storage/yyyyy GET,PUT|jobs/yyyyyy POST|upload/api/frontend/request_upload",
      "sub": "yyyyy",
      "username": "ttttt",
      "active": true
    }

### Via UserInfo endpoint (OpenID Connect Core 1.0)

Run the following cURL command:

    curl http://localhost:8080/oauth2/userinfo -H "Authorization: Bearer $access_token" | jq .
    
This should return a token UserInfo response:

    {
      "email": "xxxxx@gmail.com",
      "name": "N N",
      "sub": "yyyyyy"
    }

# Run Integration tests

The SAML integration tests currently depend on the [OpenConext/Mujina](https://github.com/OpenConext/Mujina) mock IdP. The
integration tests expects the mock IdP to be available on port 9999.

To download and run Mujina:

    git clone https://github.com/OpenConext/Mujina.git
    cd Mujina
    mvn install
    cd mujina-idp
    mvn jetty:run -DhttpPort=9999

# State
All the state is stored in a PostgreSQL database.

Very important state (should not ever be lost):

- User accounts (username/id)
- Password details (hash/salt)

Moderately important state (loss would cause inconvenience to some clients):

- Refresh tokens (and scope)

Less important state (loss should be handled automatically/trivially by the clients):

- Access tokens
- Authorization codes

Requires Oracle JDK 1.8
