package integration;

import fixtures.FixtureData;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.testing.junit.DropwizardAppRule;
import no.uit.metapipe.auth.config.MetapipeAuthServiceConfiguration;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.*;

public class LoginIntegrationTest {
    private Client client;

    @ClassRule
    public static DropwizardAppRule<MetapipeAuthServiceConfiguration> RULE = new IntegrationTestFixture();


    @Before
    public void setUp() throws InterruptedException {
        this.client = ClientBuilder.newClient();
        client.property(ClientProperties.FOLLOW_REDIRECTS, "false");
        DataSourceFactory f = RULE.getConfiguration().getDataSourceFactory();
    }

    @After
    public void tearDown() {
        this.client.close();
    }

    @Test
    public void login_view_should_be_available() {
        Response response = client.target(String.format("http://localhost:%d/login", RULE.getLocalPort()))
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    public void login_should_redirect() {
        FixtureData fixtureData = FixtureData.getDefault();
        fixtureData.apply(client, String.format("http://localhost:%d", RULE.getLocalPort()));

        Form loginForm = new Form();
        loginForm.param("username", "validUsername");
        loginForm.param("password", "validPassword123");
        loginForm.param("redirectSuccess", "http://example.com/success");
        loginForm.param("redirectFailure", "http://example.com/failure");

        Response response = client.target(String.format("http://localhost:%d/login", RULE.getLocalPort()))
                .request()
                .post(Entity.form(loginForm));

        assertThat(response.getStatus()).isEqualTo(Response.Status.SEE_OTHER.getStatusCode());
        assertThat(response.getCookies().get("metapipe_auth").toCookie().getDomain())
                .isEqualTo("metapipe.uit.no.dev"); // todo: test is in the wrong place; should go to unit

        response = client.target(String.format("http://localhost:%d/login/logout?redirect=http://example.com/loggedOut", RULE.getLocalPort()))
                .request()
                .get();
        assertThat(response.getStatus()).isEqualTo(Response.Status.SEE_OTHER.getStatusCode());
    }

    @Test
    public void registration_should_redirect() {
        Form registrationForm = new Form();
        registrationForm.param("username", "newUser");
        registrationForm.param("password", "validPassword123");
        registrationForm.param("passwordRepeat", "validPassword123");
        registrationForm.param("redirectSuccess", "http://example.com/success");
        registrationForm.param("redirectFailure", "http://example.com/failure");

        Response response = client.target(String.format("http://localhost:%d/login/register", RULE.getLocalPort()))
                .request()
                .post(Entity.form(registrationForm));

        assertThat(response.getStatus()).isEqualTo(Response.Status.SEE_OTHER.getStatusCode());
    }
}
