package integration.saml;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/*
* Code copied from https://github.com/OpenConext/Mujina
* */

@XmlRootElement
public class Attribute implements Serializable {
    public Attribute() {

    }

    public Attribute(String value) {
        List<String> v = new LinkedList<>();
        v.add(value);
        setValue(v);
    }

    private static final long serialVersionUID = 1L;

    @JsonProperty
    private List<String> value;

    public List<String> getValue() {
        return value;
    }

    @XmlElement
    public void setValue(final List<String> value) {
        this.value = value;
    }
}
