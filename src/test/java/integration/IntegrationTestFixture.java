package integration;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import no.uit.metapipe.auth.MetapipeAuthServiceApplication;
import no.uit.metapipe.auth.config.MetapipeAuthServiceConfiguration;
import no.uit.metapipe.auth.api.IntrospectionResponse;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.flywaydb.core.Flyway;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;

public class IntegrationTestFixture extends DropwizardAppRule<MetapipeAuthServiceConfiguration> {
    private Flyway flyway;

    private final String webClientId = "system_web";
    private final String webClientSecret = "74003f18714b8a46a265";

    public IntegrationTestFixture() {
        super(MetapipeAuthServiceApplication.class, ResourceHelpers.resourceFilePath("test-config.yml"));
    }

    @Override
    protected void before() {
        super.before();
        DataSourceFactory f = getConfiguration().getDataSourceFactory();
        flyway = new Flyway();
        flyway.setDataSource(f.getUrl(), f.getUser(), f.getPassword());
        flyway.migrate();
    }

    @Override
    protected void after() {
        flyway.clean();
        super.after();
    }

    public String getServerLocation() {
        return String.format("http://localhost:%d", getLocalPort());
    }

    public OAuthClient getOauthClient() {
        return new OAuthClient(new URLConnectionClient());
    }

    public OAuthAccessTokenResponse requestAccessToken() throws Exception {
        return requestAccessToken("validUsername", "validPassword123", "GET,PUT|storage/NOT_validUsername GET,PUT|storage/validUsername");
    }

    public OAuthAccessTokenResponse requestAccessToken(String username, String password, String scope) throws Exception {
        OAuthClientRequest request = tokenRequestBuilder()
                .setClientId(webClientId)
                .setClientSecret(webClientSecret)
                .setGrantType(GrantType.PASSWORD)
                .setUsername(username)
                .setPassword(password)
                .setScope(scope)
                .buildQueryMessage();
        return getOauthClient().accessToken(request);
    }

    public OAuthAccessTokenResponse requestAccessTokenByClientCredentialsGrant(String clientId, String clientSecret, String scope) throws Exception {
        OAuthClientRequest request = tokenRequestBuilder()
                .setGrantType(GrantType.CLIENT_CREDENTIALS)
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .setScope(scope)
                .buildBodyMessage();
        return getOauthClient().accessToken(request);
    }

    public IntrospectionResponse introspect(String token) {
        Form form = new Form();
        form.param("token", token);
        Client client = ClientBuilder.newClient();
        return client.target(getServerLocation() + "/oauth2/introspect")
                .request()
                .post(Entity.form(form), IntrospectionResponse.class);
    }

    public OAuthClientRequest.TokenRequestBuilder tokenRequestBuilder() {
        return OAuthClientRequest
                .tokenLocation(getServerLocation() + "/oauth2/token")
                .setClientId("asdf")
                .setClientSecret("asdf");
    }

    public String getWebClientId() {
        return webClientId;
    }

    public String getWebClientSecret() {
        return webClientSecret;
    }
}
