package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.api.UserDto;
import no.uit.metapipe.auth.core.User;
import no.uit.metapipe.auth.db.AuthenticationEventRepo;
import no.uit.metapipe.auth.db.ExternalUserIdRepo;
import no.uit.metapipe.auth.db.UserRepo;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class UserServiceTest {
    private static final UserRepo USER_REPO = mock(UserRepo.class);
    private static final ExternalUserIdRepo externalUserIdRepo = mock(ExternalUserIdRepo.class);
    private static final AuthenticationEventRepo authenticationEventRepo = mock(AuthenticationEventRepo.class);
    private static final UserService userService = new UserServiceImpl(USER_REPO, externalUserIdRepo, authenticationEventRepo);

    private static final String validUsername = "validUsername";
    private static final String nonValidUsername = "nonValidUsername";

    private User validUser;

    private User createValidUser() {
        User user = new User();
        user.setUsername(validUsername);
        return user;
    }

    @Before
    public void setup() {
        this.validUser = createValidUser();
        when(USER_REPO.findByUsername(validUsername)).thenReturn(this.validUser);
    }

    @Test
    public void should_find_valid_user_in_database() {
        Optional<UserDto> userDtoOptional = userService.findByUsername(validUsername);
        verify(USER_REPO).findByUsername(validUsername);
        assertThat(userDtoOptional.isPresent()).isTrue();
        assertThat(userDtoOptional.get().getUsername()).isEqualTo(validUsername);
    }

    @Test
    public void should_not_find_non_valid_user_in_database() {
        Optional<UserDto> userDtoOptional = userService.findByUsername(nonValidUsername);
        verify(USER_REPO).findByUsername(nonValidUsername);
        assertThat(userDtoOptional.isPresent()).isFalse();
    }
}
