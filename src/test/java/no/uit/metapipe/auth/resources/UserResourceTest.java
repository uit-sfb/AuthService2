package no.uit.metapipe.auth.resources;

import io.dropwizard.testing.junit.ResourceTestRule;
import no.uit.metapipe.auth.api.UserDto;
import no.uit.metapipe.auth.service.UserService;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;

public class UserResourceTest {
    public static final UserService userService = mock(UserService.class);

    private static final String validUsername = "validUsername";
    private static final String nonValidUsername = "nonValidUsername";
    private static final String validPassword = "validPassword";
    private UserDto validUserDto;

    private static final Optional<UserDto> noUserOptional = Optional.empty();

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new UserResource(userService))
            .build();

    @Before
    public void setup() {
        reset(userService);
        validUserDto = new UserDto();
        validUserDto.setUsername(validUsername);
        when(userService.findByUsername(validUsername)).thenReturn(Optional.of(validUserDto));
        when(userService.findByUsername(nonValidUsername)).thenReturn(noUserOptional);
    }

    @Test
    public void should_return_existing_user() {
        UserDto user = resources.client().target("/u/" + validUsername)
                .request().get(UserDto.class);
        assertThat(user.getUsername()).isEqualTo(validUsername);
        verify(userService).findByUsername(validUsername);
    }

    @Test
    public void should_fail_on_non_existing_user() {
        Response response = resources.client().target("/u/" + nonValidUsername)
                .request().get();
        assertThat(response.getStatus()).isEqualTo(404);
    }
}
