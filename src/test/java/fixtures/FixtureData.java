package fixtures;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import no.uit.metapipe.auth.api.UsernamePasswordDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static io.dropwizard.testing.FixtureHelpers.*;

import java.io.IOException;
import java.util.List;

public class FixtureData {
    private static final ObjectMapper MAPPER = new ObjectMapper(new YAMLFactory());

    public static FixtureData getDefault() {
        try {
            return MAPPER.readValue(fixture("fixtures/data.yml"), FixtureData.class);
        } catch (IOException e) {
            throw new IllegalStateException("Error reading data.", e);
        }
    }

    @JsonProperty
    private List<UsernamePasswordDto> registeredUsers;

    private Logger logger = LoggerFactory.getLogger(FixtureData.class);

    public void apply(Client client, String baseUrl) {
        Response response = client.target(baseUrl + "/u/register").request()
                .post(Entity.json(registeredUsers));
        if(response.getStatus() != 201) {
            logger.warn("Registering users failed.");
        }
    }

    public List<UsernamePasswordDto> getRegisteredUsers() {
        return registeredUsers;
    }

}
