package no.uit.metapipe.auth.core;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="AuthorizationCodes")
@NamedQueries({
        @NamedQuery(
                name = "findByAuthorizationCode",
                query = "SELECT c FROM AuthorizationCode c WHERE c.authorizationCode = :authorizationCode"
        )
})
public class AuthorizationCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String authorizationCode;
    private String state;

    @OneToOne
    Token token;

    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime validUntil;

    private boolean used;

    public boolean isValid() {
        return !isUsed(); // todo: check validUntil
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getState() {
        return state;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public void setState(String state) {
        this.state = state;
    }

    public DateTime getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(DateTime validUntil) {
        this.validUntil = validUntil;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }
}
