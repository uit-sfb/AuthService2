package no.uit.metapipe.auth.core;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table( name="Users",
        uniqueConstraints = @UniqueConstraint(columnNames = {"username"}))
@NamedQueries({
        @NamedQuery(
                name = "findUserByUsername",
                query = "SELECT u FROM User u WHERE u.username = :username"
        ),
        @NamedQuery(
                name = "findAllUsers",
                query = "SELECT u FROM User u"
        )
})
public class User {
    long id;
    String username;
    PasswordDetails passwordDetails;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    @OneToOne(orphanRemoval = true)
    public PasswordDetails getPasswordDetails() {
        return passwordDetails;
    }
    public void setPasswordDetails(PasswordDetails passwordDetails) {
        this.passwordDetails = passwordDetails;
    }
}
