package no.uit.metapipe.auth.core;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "findExternalUserId",
                query = "SELECT c from ExternalUserId c WHERE c.issuer = :issuer AND c.identifier = :identifier"
        ),
        @NamedQuery(
                name = "findExternalUserIdByUser",
                query = "SELECT c FROM ExternalUserId c WHERE c.user = :user"
        )
})
public class ExternalUserId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @OneToMany
    Set<AuthenticationEvent> authenticationEvents = new HashSet<>();

    @ManyToOne
    User user;

    String identifier;
    String issuer;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<AuthenticationEvent> getAuthenticationEvents() {
        return authenticationEvents;
    }

    public void setAuthenticationEvents(Set<AuthenticationEvent> authenticationEvents) {
        this.authenticationEvents = authenticationEvents;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
}
