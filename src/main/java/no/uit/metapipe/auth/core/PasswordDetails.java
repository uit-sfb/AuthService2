package no.uit.metapipe.auth.core;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

@Entity
@Table(name="PasswordDetails")
public class PasswordDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    byte[] passwordHash;
    byte[] passwordSalt;

    private byte[] generateHash(String password, byte[] salt) {
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return f.generateSecret(spec).getEncoded();
        } catch (Exception e) {
            IllegalStateException rte = new IllegalStateException("Error generating hash.");
            e.printStackTrace();
            throw rte;
        }
    }

    private byte[] generateSalt() {
        try {
            byte[] salt = new byte[16];
            SecureRandom random = new SecureRandom();
            random.nextBytes(salt);
            return salt;
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("Failed to generate salt.");
        }
    }

    public boolean isValidPassword(String password) {
        byte[] hash = generateHash(password, passwordSalt);
        return Arrays.equals(hash, passwordHash);
    }

    public void setPassword(String password) {
        passwordSalt = generateSalt();
        passwordHash = generateHash(password, passwordSalt);
    }
}
