package no.uit.metapipe.auth.core;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;
import java.util.Set;

@Entity
@Table( name="Tokens",
        uniqueConstraints = @UniqueConstraint(columnNames = {"tokenId"}))
@NamedQueries({
        @NamedQuery(
                name = "findByTokenId",
                query = "SELECT s FROM Token s WHERE s.tokenId = :tokenId"
        )
})
public class Token {
    private long id;

    private boolean active;

    private User subject;

    private String systemSubject;

    private User createdBy;

    private String tokenId;

    private byte[] secretHash;

    private Set<Scope> scopes;

    private boolean refreshToken;

    private long expiration;

    private AuthenticationEvent authenticationEvent;

    public static final Duration EXPIRATION_TIME = Duration.ofDays(2);

    public Token(){
       this.expiration = Instant.now().plus(EXPIRATION_TIME).toEpochMilli();
    }

    @Transient
    public boolean isSystemToken() {
        return systemSubject != null;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(nullable = false)
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }

    @JoinColumn
    @OneToOne
    public User getSubject() {
        return subject;
    }
    public void setSubject(User subject) {
        this.subject = subject;
    }

    @Column(nullable = true)
    public String getSystemSubject() {
        return systemSubject;
    }

    public void setSystemSubject(String systemSubject) {
        this.systemSubject = systemSubject;
    }

    @JoinColumn
    @OneToOne
    public User getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Column(nullable = false)
    public String getTokenId() {
        return tokenId;
    }
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @Column(nullable = false)
    public byte[] getSecretHash() {
        return secretHash;
    }
    public void setSecretHash(byte[] secretHash) {
        this.secretHash = secretHash;
    }

    @OneToMany(orphanRemoval = true)
    @JoinColumn(name="token_id")
    public Set<Scope> getScopes() {
        return scopes;
    }
    public void setScopes(Set<Scope> scopes) {
        this.scopes = scopes;
    }

    @Column(nullable = false)
    public boolean isRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(boolean refreshToken) {
        this.refreshToken = refreshToken;
    }

    @OneToOne
    @JoinColumn
    public AuthenticationEvent getAuthenticationEvent() {
        return authenticationEvent;
    }

    public void setAuthenticationEvent(AuthenticationEvent authenticationEvent) {
        this.authenticationEvent = authenticationEvent;
    }

    @NotNull
    @DecimalMin(value = "0", inclusive = false)
    public long getExpiration() {
        return expiration;
    }
    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    public byte[] hash(String secret) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            throw new IllegalStateException("Failed to obtain hashing algorithm", e);
        }
        md.update(secret.getBytes());
        return md.digest();
    }

    public String generateTokenSecret() {
        byte[] secretBytes = new byte[64];
        SecureRandom random = new SecureRandom();
        random.nextBytes(secretBytes);
        String secret = Base64.getUrlEncoder().encodeToString(secretBytes);
        setSecretHash(hash(secret));
        return secret;
    }

    public boolean validateTokenSecret(String tokenSecret) {
        return Arrays.equals(getSecretHash(), hash(tokenSecret));
    }

    @Transient
    public boolean isExpired(){
        return this.expiration < (Instant.now().toEpochMilli());
    }
}
