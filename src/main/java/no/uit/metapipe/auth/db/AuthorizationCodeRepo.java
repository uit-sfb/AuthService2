package no.uit.metapipe.auth.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.auth.core.AuthorizationCode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class AuthorizationCodeRepo extends AbstractDAO<AuthorizationCode> {
    public AuthorizationCodeRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public AuthorizationCode saveOrUpdate(AuthorizationCode authorizationCode) {
        currentSession().saveOrUpdate(authorizationCode);
        return authorizationCode;
    }

    public void delete(AuthorizationCode authorizationCode) {
        currentSession().delete(authorizationCode);
    }

    public Optional<AuthorizationCode> findByAuthorizationCode(String authorizationCode) {
        Session s = currentSession();
        Query q = s.getNamedQuery("findByAuthorizationCode")
                .setParameter("authorizationCode", authorizationCode);
        List<AuthorizationCode> authorizationCodes = list(q);
        if (authorizationCodes.size() > 0) {
            return Optional.of(authorizationCodes.get(0));
        } else {
            return Optional.empty();
        }
    }
}
