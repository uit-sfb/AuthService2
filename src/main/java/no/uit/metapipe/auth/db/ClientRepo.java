package no.uit.metapipe.auth.db;

import no.uit.metapipe.auth.config.ClientCredentialsConfig;
import no.uit.metapipe.auth.config.OAuthConfig;
import no.uit.metapipe.auth.core.Client;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Optional;

public class ClientRepo {
    final OAuthConfig oAuthConfig;

    public ClientRepo(OAuthConfig oAuthConfig) {
        this.oAuthConfig = oAuthConfig;
    }

    protected Optional<Client> findSystemClientById(String clientId) {
        for(ClientCredentialsConfig clientConfig : oAuthConfig.getClients()) {
            if(clientConfig.getClientId().equals(clientId)) {
                return Optional.of(Client.fromConfig(clientConfig));
            }
        }
        return Optional.empty();
    }

    protected Optional<Client> findUserClientById(String clientId) {
        throw new NotImplementedException();
    }

    public Optional<Client> findByClientId(String clientId) {
        Optional<Client> clientOptional = Optional.empty();
        if(clientId.startsWith("system_")) {
            clientOptional = findSystemClientById(clientId);
        } else if(clientId.startsWith("user_")) {
            clientOptional = findUserClientById(clientId);
        }
        return clientOptional;
    }

    public Optional<Client> findByClientIdAndSecret(String clientId, String clientSecret) {
        Optional<Client> clientOptional = findByClientId(clientId);
        if(!clientOptional.isPresent()) {
            return Optional.empty();
        }

        Client client = clientOptional.get();

        if(client.isValidSecret(clientSecret)) {
            return Optional.of(client);
        } else {
            return Optional.empty();
        }
    }
}
