package no.uit.metapipe.auth.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.auth.core.ExternalUserId;
import no.uit.metapipe.auth.core.User;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class ExternalUserIdRepo extends AbstractDAO<ExternalUserId> {

    public ExternalUserIdRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public ExternalUserId saveOrUpdate(ExternalUserId externalUserId) {
        currentSession().saveOrUpdate(externalUserId);
        return externalUserId;
    }

    public Optional<ExternalUserId> findById(User user) {
        Query q = currentSession().getNamedQuery("findExternalUserIdByUser").setParameter("user", user);
        List<ExternalUserId> userIds = list(q);
        return checkOutput(userIds);
    }

    public Optional<ExternalUserId> find(String issuer, String identifier) {
        Query q = currentSession().getNamedQuery("findExternalUserId")
                .setParameter("issuer", issuer)
                .setParameter("identifier", identifier);
        List<ExternalUserId> userIds = list(q);
        return checkOutput(userIds);
    }

    private Optional<ExternalUserId> checkOutput(List<ExternalUserId> userIds) {
        switch (userIds.size()) {
            case 0: return Optional.empty();
            case 1: return Optional.of(userIds.get(0));
            default: throw new IllegalStateException("More than one match for ExternalUserId");
        }
    }
}
