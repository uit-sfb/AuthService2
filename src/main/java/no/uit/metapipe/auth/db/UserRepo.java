package no.uit.metapipe.auth.db;

import io.dropwizard.hibernate.AbstractDAO;
import no.uit.metapipe.auth.core.PasswordDetails;
import no.uit.metapipe.auth.core.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserRepo extends AbstractDAO<User> {
    private static Logger logger = LoggerFactory.getLogger(UserRepo.class);

    public UserRepo(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public User findByUsername(String username) {
        Session s = currentSession();
        Query q = s.getNamedQuery("findUserByUsername")
                   .setParameter("username", username);
        if (list(q).size() > 0) {
            return list(q).get(0);
        } else {
            return null;
        }
    }

    public List<User> findAll() {
        Query q = currentSession().getNamedQuery("findAllUsers");
        return list(q);
    }

    public User saveOrUpdate(User user) {
        currentSession().saveOrUpdate(user);
        return user;
    }

    public PasswordDetails saveOrUpdate(PasswordDetails passwordDetails) {
        currentSession().saveOrUpdate(passwordDetails);
        return passwordDetails;
    }

    public User create(String username) {
        User user = new User();
        user.setUsername(username);
        currentSession().persist(user);
        return user;
    }
}
