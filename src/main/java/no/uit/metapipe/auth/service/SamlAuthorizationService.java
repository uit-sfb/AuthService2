package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.core.*;
import no.uit.metapipe.auth.db.AuthenticationEventRepo;
import no.uit.metapipe.auth.db.AuthorizationCodeRepo;
import no.uit.metapipe.auth.db.TokenRepo;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;
import no.uit.metapipe.auth.utils.SecretsGenerator;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;

import java.util.*;

public class SamlAuthorizationService {
    private final AuthenticationEventRepo authenticationEventRepo;
    private final AuthorizationCodeRepo authorizationCodeRepo;
    private final UserService userService;
    private final AuthorizationService authorizationService;
    private final TokenService tokenService;
    private final TokenRepo tokenRepo;

    public SamlAuthorizationService(AuthenticationEventRepo authenticationEventRepo, AuthorizationCodeRepo authorizationCodeRepo, UserService userService, AuthorizationService authorizationService, TokenService tokenService, TokenRepo tokenRepo) {
        this.authenticationEventRepo = authenticationEventRepo;
        this.authorizationCodeRepo = authorizationCodeRepo;
        this.userService = userService;
        this.authorizationService = authorizationService;
        this.tokenService = tokenService;
        this.tokenRepo = tokenRepo;
    }

    public AuthenticationEvent getAuthenticationEvent() {
        Authentication au = SecurityContextHolder.getContext()
                .getAuthentication();

        if (au == null) {
            throw new IllegalStateException("Authentication was null.");
        }

        SAMLCredential credential = (SAMLCredential) SecurityContextHolder
                .getContext().getAuthentication().getCredentials();

        Assertion assertion = credential.getAuthenticationAssertion();

        //String issuer = assertion.getIssuer().getValue();
        String issuer = "https://engine.elixir-czech.org/authentication/idp/metadata"; // todo: // FIXME: 10/6/16 
        //assert(issuer.equals("https://engine.elixir-czech.org/authentication/idp/metadata"));
        //String identifier = credential.getAttributeAsString("urn:oid:1.3.6.1.4.1.5923.1.1.1.6");
        String identifier = credential.getAttributeAsString("urn:oid:1.3.6.1.4.1.5923.1.1.1.13");

        ExternalUserId externalUserId = userService.findOrCreateUserByExternalId(issuer, identifier);

        // Authentication event
        AuthenticationEvent authenticationEvent = new AuthenticationEvent();
        authenticationEvent.setIssuer(issuer);
        authenticationEvent.setExternalUserId(externalUserId);

        Map<String, List<String>> attributes = new HashMap<>();
        for(Attribute attr : credential.getAttributes()) {
            String name = attr.getName();
            String[] value = credential.getAttributeAsStringArray(name);
            attributes.put(name, Arrays.asList(value));
        }
        authenticationEvent.setAttributesList(attributes);
        authenticationEventRepo.saveOrUpdate(authenticationEvent);
        return authenticationEvent;
    }

    public AuthorizationCode createAuthorizationCode(Set<String> requestedScopesStr) {
        OAuthResponseDetails responseDetails = getAccessToken(requestedScopesStr, true);

        tokenService.invalidateByAccessToken(responseDetails.getAccessToken());

        // todo: refactor later
        Token refreshToken = tokenService.getByTokenString(responseDetails.getRefreshTokenOptional().get()).get();

        // link authorization code to refresh token
        AuthorizationCode authorizationCode = new AuthorizationCode();
        authorizationCode.setAuthorizationCode(new SecretsGenerator().createSecret(10));
        authorizationCode.setToken(refreshToken);

        authorizationCodeRepo.saveOrUpdate(authorizationCode);

        return authorizationCode;
    }

    public OAuthResponseDetails getAccessToken(Set<String> requestedScopesStr, boolean createRefreshToken) {
        AuthenticationEvent authenticationEvent = getAuthenticationEvent();
        User user = authenticationEvent.getExternalUserId().getUser();
        OAuthResponseDetails responseDetails = authorizationService.authorize(Optional.empty(), Optional.of(user), requestedScopesStr, createRefreshToken, Optional.of(authenticationEvent));
        Token accessToken = tokenService.getByTokenString(responseDetails.getAccessToken()).get();
        accessToken.setAuthenticationEvent(authenticationEvent);
        tokenRepo.saveOrUpdate(accessToken);
        if(createRefreshToken) {
            Token refreshToken = tokenService.getByTokenString(responseDetails.getRefreshTokenOptional().get()).get();
            refreshToken.setAuthenticationEvent(authenticationEvent);
            tokenRepo.saveOrUpdate(refreshToken);
        }
        return responseDetails;
    }
}
