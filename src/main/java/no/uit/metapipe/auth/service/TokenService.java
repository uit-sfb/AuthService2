package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.api.AccessToken;
import no.uit.metapipe.auth.api.IntrospectionResponse;
import no.uit.metapipe.auth.core.Client;
import no.uit.metapipe.auth.core.Scope;
import no.uit.metapipe.auth.core.Token;
import no.uit.metapipe.auth.core.User;

import java.util.Optional;
import java.util.Set;

public interface TokenService {
    Optional<Token> getByTokenString(String tokenStr);

    Optional<String> authenticateUsernamePassword(String username, String password);

    IntrospectionResponse introspect(String accessToken);

    void invalidateByAccessToken(String token);

    AccessToken createTokenForUser(User sub, Set<Scope> scopes, boolean refreshToken);

    AccessToken createTokenForClient(Client client, Set<Scope> scopes);
}
