package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.api.UserDto;
import no.uit.metapipe.auth.api.UserInfo;
import no.uit.metapipe.auth.core.AuthenticationEvent;
import no.uit.metapipe.auth.core.ExternalUserId;
import no.uit.metapipe.auth.core.PasswordDetails;
import no.uit.metapipe.auth.core.User;
import no.uit.metapipe.auth.db.AuthenticationEventRepo;
import no.uit.metapipe.auth.db.ExternalUserIdRepo;
import no.uit.metapipe.auth.db.UserRepo;
import no.uit.metapipe.auth.utils.SecretsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.AuthenticationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepo userRepo;
    private final ExternalUserIdRepo externalUserIdRepo;
    private final AuthenticationEventRepo authenticationEventRepo;

    public UserServiceImpl(UserRepo userRepo, ExternalUserIdRepo externalUserIdRepo, AuthenticationEventRepo authenticationEventRepo) {
        this.userRepo = userRepo;
        this.externalUserIdRepo = externalUserIdRepo;
        this.authenticationEventRepo = authenticationEventRepo;
    }

    @Override
    public UserDto createUser(String username) {
        User user = userRepo.create(username);
        logger.info("Created user: " + username);
        UserDto userDto = new UserDto();
        userDto.setUsername(username);
        return userDto;
    }

    @Override
    public Optional<UserDto> findByUsername(String username) {
        User user;
        try {
            user = userRepo.findByUsername(username);
            if(user != null) {
                UserDto userDto = new UserDto();
                userDto.setUsername(user.getUsername());
                return Optional.of(userDto);
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public List<UserDto> listUsers() {
        List<User> users = userRepo.findAll();
        List<UserDto> userDtos = new ArrayList<>(users.size());
        for(User u : users) {
            UserDto userDto = new UserDto();
            userDto.setUsername(u.getUsername());
            userDtos.add(userDto);
        }
        return userDtos;
    }

    @Override
    public UserDto registerUsernamePassword(String username, String password) {
        User user = new User();
        user.setUsername(username);

        PasswordDetails passwordDetails = new PasswordDetails();
        passwordDetails.setPassword(password);
        user.setPasswordDetails(passwordDetails);

        userRepo.saveOrUpdate(user);
        userRepo.saveOrUpdate(passwordDetails);

        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        return userDto;
    }

    public ExternalUserId findOrCreateUserByExternalId(String issuer, String identifier) {
        Optional<ExternalUserId> externalUserIdOptional = externalUserIdRepo.find(issuer, identifier);
        if(externalUserIdOptional.isPresent()) {
            return externalUserIdOptional.get();
        } else {
            User user = new User();
            user.setUsername(new SecretsGenerator().createSecret(5));

            ExternalUserId externalUserId = new ExternalUserId();
            externalUserId.setUser(user);
            externalUserId.setIssuer(issuer);
            externalUserId.setIdentifier(identifier);

            userRepo.saveOrUpdate(user);
            externalUserIdRepo.saveOrUpdate(externalUserId);
            return externalUserId;
        }
    }

    @Override
    public List<UserInfo> getUsersFromNames(List<String> userNames) throws AuthenticationException {

        if (userNames == null) {
            return new ArrayList<>();
        }

        ArrayList<UserInfo> userInfos = new ArrayList<>();
        for (String name : userNames) {
            User user = userRepo.findByUsername(name);
            try {
                if (user != null) {
                    Optional<ExternalUserId> externalUserIdOptional = externalUserIdRepo.findById(user);
                    if (externalUserIdOptional.isPresent()) {
                        ExternalUserId externalUserId = externalUserIdOptional.get();
                        Optional<List<AuthenticationEvent>> authenticationEventsOptional = authenticationEventRepo.findByExternalUserId(externalUserId);
                        if (authenticationEventsOptional.isPresent()) {
                            List<AuthenticationEvent> authenticationEvents = authenticationEventsOptional.get();
                            AuthenticationEvent authenticationEvent = authenticationEvents.iterator().next();
                            UserInfo userInfo = new UserInfo();
                            Map<String, List<String>> attributes = authenticationEvent.getAttributesList();
                            userInfo.setEmail(attributes.get("urn:oid:0.9.2342.19200300.100.1.3") != null
                                    ? attributes.get("urn:oid:0.9.2342.19200300.100.1.3").get(0) : null);
                            userInfo.setName(attributes.get("urn:oid:2.16.840.1.113730.3.1.241") != null
                                    ? attributes.get("urn:oid:2.16.840.1.113730.3.1.241").get(0) : null);
                            userInfo.setSub(name);
                            userInfo.setOrganization(attributes.get("urn:oid:1.3.6.1.4.1.25178.1.2.9") != null
                                    ? attributes.get("urn:oid:1.3.6.1.4.1.25178.1.2.9").get(0) : null);
                            userInfo.setOrganizationAffiliations(attributes.get("urn:oid:1.3.6.1.4.1.5923.1.1.1.9"));
                            userInfos.add(userInfo);
                        }
                    }
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
        return userInfos;
    }
}
