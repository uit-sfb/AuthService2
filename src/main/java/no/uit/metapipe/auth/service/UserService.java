package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.api.UserDto;
import no.uit.metapipe.auth.api.UserInfo;
import no.uit.metapipe.auth.core.ExternalUserId;

import javax.naming.AuthenticationException;
import java.util.List;
import java.util.Optional;

public interface UserService {
    UserDto createUser(String username);
    Optional<UserDto> findByUsername(String username);
    List<UserDto> listUsers();
    UserDto registerUsernamePassword(String username, String password);
    ExternalUserId findOrCreateUserByExternalId(String issuer, String identifier);

    List<UserInfo> getUsersFromNames(List<String> userNames) throws AuthenticationException;
}
