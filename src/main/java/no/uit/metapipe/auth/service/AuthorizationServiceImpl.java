package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.api.AccessToken;
import no.uit.metapipe.auth.core.*;
import no.uit.metapipe.auth.db.AuthorizationCodeRepo;
import no.uit.metapipe.auth.db.ScopeRepo;
import no.uit.metapipe.auth.db.TokenRepo;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;
import no.uit.metapipe.auth.utils.TokenFactory;
import no.uit.metapipe.auth.utils.UriScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class AuthorizationServiceImpl implements AuthorizationService {
    private final ScopeRepo scopeRepo;
    private final TokenRepo tokenRepo;
    private final TokenService tokenService;
    private final TokenFactory tokenFactory = new TokenFactory();
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final AuthorizationCodeRepo authorizationCodeRepo;

    public AuthorizationServiceImpl(ScopeRepo scopeRepo, TokenRepo tokenRepo, TokenService tokenService, AuthorizationCodeRepo authorizationCodeRepo) {
        this.scopeRepo = scopeRepo;
        this.tokenRepo = tokenRepo;
        this.tokenService = tokenService;
        this.authorizationCodeRepo = authorizationCodeRepo;
    }

    private String scopeSetAsString(Set<String> scopeSet) {
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        for(String s : scopeSet) {
            if(!first) {
                builder.append(' ');
            }
            builder.append(s);
            first = false;
        }
        return builder.toString();
    }

    @Override
    public OAuthResponseDetails authorize(Optional<Client> clientOptional, Optional<User> userOptional, Set<String> scopeSet, boolean getRefreshToken, Optional<AuthenticationEvent> authenticationEvent) {
        boolean isAdmin = false;
        if(authenticationEvent.isPresent()) {
            isAdmin = checkAdmin(authenticationEvent.get());
        }

        if(!clientOptional.isPresent() && userOptional.isPresent()) {
            OAuthResponseDetails res = authorizeUser(userOptional.get(), scopeSet, getRefreshToken, isAdmin);
            logger.info(String.format("Authorized user '%s' (unknown client). Requested scope: '%s', granted scope: '%s'.",
                userOptional.get().getUsername(), scopeSetAsString(scopeSet), scopeSetAsString(res.getScope())));
            return res;
        }
        if(clientOptional.isPresent() && !userOptional.isPresent()) {
            if(getRefreshToken) {
                throw new IllegalArgumentException("Refresh token cannot be requested for clients.");
            }
            OAuthResponseDetails res = authorizeClient(clientOptional.get(), scopeSet);
            logger.info(String.format("Authorized client '%s' on behalf of itself. Requested scope: '%s', granted scope: '%s'.",
                    clientOptional.get().getClientId(), scopeSetAsString(scopeSet), scopeSetAsString(res.getScope())));
            return res;
        }
        throw new IllegalArgumentException();
    }

    private boolean checkAdmin(AuthenticationEvent authenticationEvent) {
        boolean isAdmin = false;

        Map<String, List<String>> attributes = authenticationEvent.getAttributesList();
        for (Map.Entry<String, List<String>> entry : attributes.entrySet()) {
            for (String value : entry.getValue()) {
                String subString = value.substring(value.indexOf(":")+1);
                if (subString.equals("Meta-pipe:admin")) {
                    isAdmin = true;
                }
            }
        }
        return isAdmin;
    }

    @Override
    public Set<Scope> scopeIntersection(Set<Scope> allowedScopes, Set<Scope> requestedScope) {
        HashSet<Scope> finalScope = new HashSet<>();

        Set<UriScope> allowedUriScopes = new HashSet<>();
        for(Scope s: allowedScopes) {
            allowedUriScopes.add(s.toUriScope());
        }
        Set<UriScope> requestedUriScopes = new HashSet<>();
        for(Scope s: requestedScope) {
            requestedUriScopes.add(s.toUriScope());
        }

        for(UriScope uriScope : UriScope.confineRequestedScopes(allowedUriScopes, requestedUriScopes)) {
            finalScope.add(Scope.fromUriScope(uriScope));
        }

        return finalScope;
    }

    public OAuthResponseDetails authorizeClient(Client client, Set<String> requestedScopeStr) {
        Set<Scope> requestedScopes = new HashSet<>();
        for (String scopeStr : requestedScopeStr) {
            requestedScopes.add(Scope.fromString(scopeStr));
        }

        Set<Scope> finalScope;
        if(requestedScopeStr.isEmpty()) {
            finalScope = client.getScopes();
        } else {
            finalScope = scopeIntersection(client.getScopes(), requestedScopes);
        }

        AccessToken accessToken = tokenService.createTokenForClient(client, finalScope);

        Set<String> finalScopeStr = new HashSet<>();
        for (Scope s : finalScope) {
            finalScopeStr.add(s.encodeString());
        }

        OAuthResponseDetails result = new OAuthResponseDetails();
        result.setScope(finalScopeStr);
        result.setAccessToken(accessToken.getToken());
        return result;
    }

    public OAuthResponseDetails authorizeUser(User user, Set<String> requestedScopeStr, boolean getRefreshToken, boolean admin) {
        Set<Scope> requestedScopes = new HashSet<>();
        for (String scopeStr : requestedScopeStr) {
            requestedScopes.add(Scope.fromString(scopeStr));
        }

        // Authorize
        Set<Scope> finalScope;
        if(requestedScopeStr.isEmpty()) {
            finalScope = getDefaultUserScope(user, admin);
        } else {
            finalScope = scopeIntersection(getMaxUserScope(user, admin), requestedScopes);
        }

        // Encode scope
        Set<String> finalScopeStr = new HashSet<>();
        for (Scope s : finalScope) {
            finalScopeStr.add(s.encodeString());
        }

        OAuthResponseDetails result = new OAuthResponseDetails();
        result.setScope(finalScopeStr);
        AccessToken accessToken = tokenService.createTokenForUser(user, finalScope, false);
        result.setAccessToken(accessToken.getToken());

        if (getRefreshToken) {
            AccessToken refreshToken = tokenService.createTokenForUser(user, finalScope, true);
            result.setRefreshToken(refreshToken.getToken());
        }

        return result;
    }

    protected Set<Scope> getMaxUserScope(User user, boolean admin) {
        String subjectId = user.getUsername();
        Scope storage = new Scope();
        Set<Scope> scopes = new HashSet<>();
        storage.setUri(String.format("storage/%s", subjectId));
        storage.addMethods("GET", "POST", "PUT", "HEAD");

        if(admin) {
            Scope adminJobApi = new Scope();
            adminJobApi.setUri("jobs/");
            adminJobApi.addMethods("GET", "PUT", "POST", "PATCH", "HEAD", "DELETE");
            scopes.add(adminJobApi);
        }
        Scope jobApi = new Scope();
        jobApi.setUri(String.format("jobs/users/%s", subjectId));
        jobApi.addMethods("GET", "PUT");


        Scope upload = new Scope();
        upload.setUri("upload/api/frontend/request_upload");
        upload.addMethods("POST");

        scopes.add(storage);
        scopes.add(jobApi);
        scopes.add(upload);
        return scopes;
    }

    private Set<Scope> getDefaultUserScope(User user, boolean admin) {
        return getMaxUserScope(user, admin);
    }

    @Override
    public Token refresh(Token refreshToken) {
        if(!refreshToken.isRefreshToken()) {
            throw new IllegalArgumentException("token is not a refresh token");
        }

        if(!refreshToken.isActive()) {
            throw new IllegalArgumentException("refresh token is not active");
        }

        Token accessToken = new TokenFactory().createToken();
        accessToken.setActive(true);
        accessToken.generateTokenSecret(); // // TODO: this should not happen here
        accessToken.setSubject(refreshToken.getSubject());

        HashSet<Scope> scopes = new HashSet<>();
        scopes.addAll(refreshToken.getScopes());

        accessToken.setScopes(scopes);
        accessToken.setAuthenticationEvent(refreshToken.getAuthenticationEvent());
        accessToken.setCreatedBy(refreshToken.getCreatedBy());
        // accessToken.setExpiration(); // todo
        // todo: add parent at some point

        tokenRepo.saveOrUpdate(accessToken);
        return accessToken;
    }

    @Override
    public OAuthResponseDetails authorizeCode(String authorizationCodeStr, boolean getRefreshToken) {
        Optional<AuthorizationCode> authorizationCodeOptional = authorizationCodeRepo.findByAuthorizationCode(authorizationCodeStr);
        if(!authorizationCodeOptional.isPresent()) {
            throw new IllegalArgumentException("Authorization code is not valid (not found)"); // todo: more meaningful exception
        }

        AuthorizationCode authorizationCode = authorizationCodeOptional.get();

        if(!authorizationCode.isValid()) {
            throw new IllegalArgumentException("Authorization code is not valid (does not validate)"); // todo: more meaningful exception
        }

        authorizationCode.setUsed(true);
        authorizationCodeRepo.saveOrUpdate(authorizationCode);

        // get refresh token

        Token refreshToken = authorizationCode.getToken();
        Token accessToken = refresh(refreshToken);


        // return access token (and possibly also a refresh token)

        OAuthResponseDetails responseDetails = new OAuthResponseDetails();
        responseDetails.setAccessToken(new AccessToken(accessToken.getTokenId(), accessToken.generateTokenSecret()).getToken());
        tokenRepo.saveOrUpdate(accessToken);
        if(getRefreshToken) {
            responseDetails.setRefreshToken(new AccessToken(refreshToken.getTokenId(), refreshToken.generateTokenSecret()).getToken());
            tokenRepo.saveOrUpdate(refreshToken);
        }

        Set<String> scopeStr = new HashSet<>();
        for(Scope scope : accessToken.getScopes()) {
            scopeStr.add(scope.encodeString());
        }

        responseDetails.setScope(scopeStr);

        return responseDetails;
    }
}
