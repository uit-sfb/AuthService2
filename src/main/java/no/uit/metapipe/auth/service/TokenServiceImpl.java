package no.uit.metapipe.auth.service;

import no.uit.metapipe.auth.config.OAuthConfig;
import no.uit.metapipe.auth.api.AccessToken;
import no.uit.metapipe.auth.api.IntrospectionResponse;
import no.uit.metapipe.auth.core.*;
import no.uit.metapipe.auth.db.ScopeRepo;
import no.uit.metapipe.auth.db.TokenRepo;
import no.uit.metapipe.auth.db.UserRepo;
import no.uit.metapipe.auth.utils.TokenFactory;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import java.util.HashSet;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;

public class TokenServiceImpl implements TokenService {
    private final UserRepo userRepo;
    private final TokenRepo tokenRepo;
    private final ScopeRepo scopeRepo;
    private final TokenFactory tokenFactory;
    private final OAuthConfig oAuthConfig;

    public TokenServiceImpl(UserRepo userRepo, TokenRepo tokenRepo, ScopeRepo scopeRepo, TokenFactory tokenFactory, OAuthConfig oAuthConfig) {
        this.userRepo = userRepo;
        this.tokenRepo = tokenRepo;
        this.scopeRepo = scopeRepo;
        this.tokenFactory = tokenFactory;
        this.oAuthConfig = oAuthConfig;
    }

    @Override
    public Optional<Token> getByTokenString(String tokenStr) {
        AccessToken accessToken = AccessToken.fromString(tokenStr);
        Optional<Token> tokenOptional = tokenRepo.findByTokenId(accessToken.getTokenId());
        if(tokenOptional.isPresent()) {
            Token token = tokenOptional.get();
            if(token.isActive() && token.validateTokenSecret(accessToken.getTokenSecret()) && !token.isExpired()) {
                return Optional.of(token);
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<String> authenticateUsernamePassword(String username, String password) {
        User subject = userRepo.findByUsername(username);
        PasswordDetails passwordDetails = subject.getPasswordDetails();
        if(passwordDetails.isValidPassword(password)) {
            Token token = tokenFactory.createToken();
            String secret = token.generateTokenSecret();
            token.setSubject(subject);
            token.setCreatedBy(subject);
            token.setActive(true);
            token.setExpiration(Instant.now().toEpochMilli() + oAuthConfig.getAccessTokenExpirationMillis());
            tokenRepo.saveOrUpdate(token);

            AccessToken accessToken = new AccessToken(token.getTokenId(), secret);
            return Optional.of(accessToken.getToken());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public IntrospectionResponse introspect(String accessTokenStr) {
        IntrospectionResponse response = new IntrospectionResponse();
        try {
            AccessToken.fromString(accessTokenStr);
        } catch (Exception e) {
            response.setActive(false);
            return response;
        }
        Optional<Token> tokenOptional = getByTokenString(accessTokenStr);
        if(tokenOptional.isPresent()) {
            Token token = tokenOptional.get();
            assert(token.isActive());

            StringBuilder scope = new StringBuilder();
            for(Scope s : token.getScopes()) {
                if (scope.length() != 0) {
                    scope.append(' ');
                }
                scope.append(s.encodeString());
            }

            if(token.isSystemToken()) {
                response.setSub(token.getSystemSubject());
            } else {
                response.setSub(token.getSubject().getUsername());
                response.setUsername(token.getSubject().getUsername());
            }
            response.setScope(scope.toString());

            response.setActive(token.isActive());
        } else {
            response.setActive(false);
        }
        return response;
    }

    @Override
    public void invalidateByAccessToken(String tokenStr) {
        Optional<Token> tokenOptional = getByTokenString(tokenStr);
        if(tokenOptional.isPresent()) {
                Token token = tokenOptional.get();
                token.setActive(false);
        }
    }

    @Override
    public AccessToken createTokenForUser(User sub, Set<Scope> scopes, boolean refreshToken) {
        Set<Scope> copiedScopes = new HashSet<>();
        for (Scope scope : scopes) {
            try {
                copiedScopes.add( (Scope) scope.clone());
            } catch (CloneNotSupportedException e) {
                throw new IllegalStateException("Failure during call to clone", e);
            }
        }

        Token token = tokenFactory.createToken();
        token.setSubject(sub);
        token.setCreatedBy(sub);
        token.setActive(true);
        token.setRefreshToken(refreshToken);

        if(refreshToken) {
            token.setExpiration(Instant.now().toEpochMilli() + oAuthConfig.getRefreshTokenExpirationMillis());
        } else {
            token.setExpiration(Instant.now().toEpochMilli() + oAuthConfig.getAccessTokenExpirationMillis());
        }

        AccessToken accessToken = new AccessToken(token.getTokenId(), token.generateTokenSecret());

        tokenRepo.save(token);

        // Persist
        for(Scope s : copiedScopes) {
            s.setToken(token);
        }
        token.setScopes(copiedScopes);

        for(Scope s : copiedScopes) {
            scopeRepo.saveOrUpdate(s);
        }

        tokenRepo.update(token);
        return accessToken;
    }

    @Override
    public AccessToken createTokenForClient(Client client, Set<Scope> scopes) {
        Set<Scope> copiedScopes = new HashSet<>();
        for (Scope scope : scopes) {
            try {
                copiedScopes.add( (Scope) scope.clone());
            } catch (CloneNotSupportedException e) {
                throw new IllegalStateException("Failure during call to clone", e);
            }
        }

        Token token = tokenFactory.createToken();
        token.setActive(true);
        token.setRefreshToken(false);

        if(client.isSystemClient()) {
            token.setSystemSubject(client.getClientId());
        } else {
            throw new NotImplementedException();
        }

        AccessToken accessToken = new AccessToken(token.getTokenId(), token.generateTokenSecret());

        tokenRepo.save(token);

        // Persist
        for(Scope s : copiedScopes) {
            s.setToken(token);
        }
        token.setScopes(copiedScopes);

        for(Scope s : copiedScopes) {
            scopeRepo.saveOrUpdate(s);
        }

        tokenRepo.update(token);
        return accessToken;
    }
}
