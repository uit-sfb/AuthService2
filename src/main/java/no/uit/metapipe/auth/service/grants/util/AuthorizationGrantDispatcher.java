package no.uit.metapipe.auth.service.grants.util;

import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class AuthorizationGrantDispatcher implements AuthorizationGrantService {
    private Map<String, AuthorizationGrantService> authorizationGrants = new HashMap<>();

    public AuthorizationGrantDispatcher add(AuthorizationGrantService authorizationGrant) {
        authorizationGrants.put(authorizationGrant.getGrantType(), authorizationGrant);
        return this;
    }

    @Override
    public OAuthResponse authorize(OAuthTokenRequest tokenRequest) throws OAuthSystemException {
        AuthorizationGrantService grantService = authorizationGrants.get(tokenRequest.getGrantType());
        if(grantService == null) {
            return OAuthASResponse
                    .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                    .setError(OAuthError.TokenResponse.UNSUPPORTED_GRANT_TYPE)
                    .setErrorDescription("Unsupported grant")
                    .buildJSONMessage();
        }
        return grantService.authorize(tokenRequest);
    }

    @Override
    public String getGrantType() {
        return null;
    }
}
