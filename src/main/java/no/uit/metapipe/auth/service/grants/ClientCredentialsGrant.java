package no.uit.metapipe.auth.service.grants;

import no.uit.metapipe.auth.core.Client;
import no.uit.metapipe.auth.db.ClientRepo;
import no.uit.metapipe.auth.service.AuthorizationService;
import no.uit.metapipe.auth.service.grants.util.AbstractAuthorizationGrantService;
import no.uit.metapipe.auth.service.grants.util.OAuthResponseDetails;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

import java.util.Optional;

public class ClientCredentialsGrant extends AbstractAuthorizationGrantService {
    final AuthorizationService authorizationService;
    final ClientRepo clientRepo;

    public ClientCredentialsGrant(AuthorizationService authorizationService, ClientRepo clientRepo) {
        super(clientRepo);
        this.authorizationService = authorizationService;
        this.clientRepo = clientRepo;
    }

    @Override
    public OAuthResponse authorize(OAuthTokenRequest request, Client client) throws OAuthSystemException {
        // todo: by username/password
        Optional<Client> clientOptional;
        if(request.getUsername() != null) {
            clientOptional = clientRepo.findByClientIdAndSecret(request.getUsername(), request.getPassword());
        } else {
            clientOptional = clientRepo.findByClientIdAndSecret(request.getClientId(), request.getClientSecret());
        }
        if(clientOptional.isPresent()) {
            OAuthResponseDetails responseDetails =
                    authorizationService.authorize(clientOptional, Optional.empty(), request.getScopes(), false, Optional.empty());
            return tokenResponseBuilder(responseDetails).buildJSONMessage();
        } else {
            return invalidGrant("Invalid client credentials.");
        }
    }

    @Override
    public String getGrantType() {
        return "client_credentials";
    }
}
