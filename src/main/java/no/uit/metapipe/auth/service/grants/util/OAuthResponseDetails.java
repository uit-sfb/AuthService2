package no.uit.metapipe.auth.service.grants.util;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class OAuthResponseDetails {
    private String accessToken;
    private Optional<String> refreshTokenOptional = Optional.empty();
    private Set<String> scope = new HashSet<>();

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Optional<String> getRefreshTokenOptional() {
        return refreshTokenOptional;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshTokenOptional = Optional.of(refreshToken);
    }

    public Set<String> getScope() {
        return scope;
    }

    public void setScope(Set<String> scope) {
        this.scope = scope;
    }
}
