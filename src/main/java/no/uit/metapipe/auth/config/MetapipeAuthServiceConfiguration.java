package no.uit.metapipe.auth.config;

import com.fasterxml.jackson.jaxrs.json.annotation.JSONP;
import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.flyway.FlywayFactory;
import no.uit.metapipe.auth.utils.saml.SamlConfig;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class MetapipeAuthServiceConfiguration extends Configuration {

    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @Valid
    @NotNull
    @JsonProperty
    private String externalUrl;

    @Valid
    @NotNull
    @JsonProperty
    private AuthenticationCookieConfig authCookie = new AuthenticationCookieConfig();

    @Valid
    @NotNull
    @JsonProperty
    private FlywayFactory flyway = new FlywayFactory();

    @Valid
    @NotNull
    @JsonProperty
    private SamlConfig saml = new SamlConfig();

    @Valid
    @NotNull
    @JsonProperty
    private OAuthConfig oauth = new OAuthConfig();

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public AuthenticationCookieConfig getAuthCookie() {
        return authCookie;
    }

    public FlywayFactory getFlywayFactory() {
        return flyway;
    }

    public SamlConfig getSamlConfig() {
        return saml;
    }

    public OAuthConfig getOAuthConfig() {
        return oauth;
    }
}
