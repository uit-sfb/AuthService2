package no.uit.metapipe.auth.config;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class OAuthConfig {

    @NotNull
    @JsonProperty
    private String accessTokenExpiration = "2 h";

    @NotNull
    @JsonProperty
    private String refreshTokenExpiration = "2 d";

    private List<ClientCredentialsConfig> clients = new LinkedList<>();

    private final String invalidFormat = "Invalid format, cannot parse the parameter. Valid format is: '<number><space><timeunit>'. TimeUnit must be 'd', 'h', 'm', 's' or 'ms'. For example '10 d', '376 m', etc.";

    private long parse(String expiration) {
        if(expiration == null) {
            throw new IllegalArgumentException("Empty parameter");
        }
        String[] tokens = expiration.split(" ");
        if(tokens.length != 2) {
            throw new IllegalArgumentException(invalidFormat);
        }
        String unit = tokens[1];
        long res;
        try {
            res = Long.parseLong(tokens[0]);
        } catch(NumberFormatException e) {
            throw new IllegalArgumentException(invalidFormat, e);
        }

        switch(unit) {
            case "ms":
                break;
            case "s":
                res = TimeUnit.SECONDS.toMillis(res);
                break;
            case "m":
                res = TimeUnit.MINUTES.toMillis(res);
                break;
            case "h":
                res = TimeUnit.HOURS.toMillis(res);
                break;
            case "d":
                res = TimeUnit.DAYS.toMillis(res);
                break;
            default:
                throw new IllegalArgumentException(invalidFormat);
        }
        return res;
    }

    public long getAccessTokenExpirationMillis() {
        return parse(accessTokenExpiration);
    }

    public long getRefreshTokenExpirationMillis() {
        return parse(refreshTokenExpiration);
    }

    public List<ClientCredentialsConfig> getClients() {
        return clients;
    }
}
