package no.uit.metapipe.auth.config;

public class AuthenticationCookieConfig {
    public static Builder builder() {
        return new Builder();
    }

    private String cookieName;
    private String cookieDomain;

    public String getCookieName() {
        return cookieName;
    }

    public String getCookieDomain() {
        return cookieDomain;
    }

    public static class Builder {
        AuthenticationCookieConfig config = new AuthenticationCookieConfig();

        public Builder cookieName(String cookieName) {
            config.cookieName = cookieName;
            return this;
        }

        public Builder cookieDomain(String domain) {
            config.cookieDomain = domain;
            return this;
        }

        public AuthenticationCookieConfig build() {
            if(config == null) throw new IllegalStateException("Builder already built");
            AuthenticationCookieConfig ret = config;
            config = null;
            return ret;
        }
    }
}
