package no.uit.metapipe.auth.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.uit.metapipe.auth.utils.SecretsGenerator;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.LinkedList;
import java.util.List;

public class ClientCredentialsConfig {
    String clientId;
    String clientSecretHash;

    @JsonIgnore
    String generatedSecret;

    List<String> scopes = new LinkedList<>();

    List<String> redirects = new LinkedList<>();

    List<String> grants = new LinkedList<>();

    public static ClientCredentialsConfig createFromClientId(String clientId, int secretLength) {
        ClientCredentialsConfig config = new ClientCredentialsConfig();
        config.clientId = clientId;

        SecretsGenerator secretsGenerator = new SecretsGenerator();
        config.generatedSecret = secretsGenerator.createSecret(secretLength);
        config.clientSecretHash = DigestUtils.sha256Hex(config.generatedSecret);
        return config;
    }

    public boolean compareSecret(String secret) {
        String hash = DigestUtils.sha256Hex(secret);
        return hash.equals(clientSecretHash);
    }

    public String getGeneratedSecret() {
        if (generatedSecret == null) {
            throw new IllegalStateException("getGeneratedSecret() can only be called on a freshly generated config.");
        }
        return generatedSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public String getClientSecretHash() {
        return clientSecretHash;
    }

    public List<String> getRedirects() {
        return redirects;
    }

    public List<String> getGrants() {
        return grants;
    }
}
