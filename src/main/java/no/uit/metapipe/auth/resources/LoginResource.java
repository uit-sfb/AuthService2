package no.uit.metapipe.auth.resources;

import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import no.uit.metapipe.auth.config.AuthenticationCookieConfig;
import no.uit.metapipe.auth.service.TokenService;
import no.uit.metapipe.auth.service.UserService;
import no.uit.metapipe.auth.view.LoginFormView;
import no.uit.metapipe.auth.view.UserRegistrationFormView;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Cookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;


@Path("/login")
public class LoginResource {
    private final AuthenticationCookieConfig authenticationCookieConfig;
    private final UserService userService;
    private final TokenService tokenService;

    public LoginResource(AuthenticationCookieConfig authenticationCookieConfig, UserService userService, TokenService tokenService) {
        this.authenticationCookieConfig = authenticationCookieConfig;
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @GET
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public LoginFormView loginView(
            @QueryParam("redirectSuccess") String redirectSuccess,
            @QueryParam("redirectFailure") String redirectFailure) {
        if(redirectSuccess == null) redirectSuccess = "";
        if(redirectFailure == null) redirectFailure = "";
        LoginFormView view = new LoginFormView();
        view.setRedirectSuccess(redirectSuccess);
        view.setRedirectFailure(redirectFailure);
        view.setAction("/login");
        return view;
    }

    @POST
    @Timed
    @UnitOfWork
    public Response login(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("redirectSuccess") String redirectSuccess,
            @FormParam("redirectFailure") String redirectFailure) throws URISyntaxException {
        Optional<String> accessTokenOptional = tokenService.authenticateUsernamePassword(username, password);
        if (accessTokenOptional.isPresent()) {
            String accessToken = accessTokenOptional.get();
            Cookie tokenCookie = new Cookie(authenticationCookieConfig.getCookieName(), accessToken, "/", authenticationCookieConfig.getCookieDomain());
            NewCookie newCookie = new NewCookie(tokenCookie);
            return Response.seeOther(new URI(redirectSuccess)).cookie(newCookie).build();
        } else {
            return Response.seeOther(new URI(redirectFailure)).build();
        }
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/logout")
    public Response logout(@QueryParam("redirect") String redirect, @Context ContainerRequestContext request) throws URISyntaxException {
        String cookieName = authenticationCookieConfig.getCookieName();
        Map<String, Cookie> cookies = request.getCookies();
        if(cookies.containsKey(cookieName)) {
            Cookie cookie = cookies.get(cookieName);
            String token = cookie.getValue();
            tokenService.invalidateByAccessToken(token);
        } else {

        }
        return Response.seeOther(new URI(redirect)).build();
    }

    @GET
    @Path("/register")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public UserRegistrationFormView registerView(
            @QueryParam("redirectSuccess") String redirectSuccess,
            @QueryParam("redirectFailure") String redirectFailure) {
        UserRegistrationFormView view = new UserRegistrationFormView();
        if(redirectSuccess == null) redirectSuccess = "";
        if(redirectFailure == null) redirectFailure = "";
        view.setRedirectSuccess(redirectSuccess);
        view.setRedirectFailure(redirectFailure);
        view.setAction("/login/register");
        return view;
    }

    @POST
    @UnitOfWork
    @Timed
    @Path("/register")
    public Response register(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("passwordRepeat") String passwordRepeat,
            @FormParam("redirectSuccess") String redirectSuccess,
            @FormParam("redirectFailure") String redirectFailure) throws URISyntaxException {
        if(!password.equals(passwordRepeat)) {
            return Response.seeOther(new URI(redirectFailure)).build();
        } else {
            userService.registerUsernamePassword(username, password);
            String accessToken = tokenService.authenticateUsernamePassword(username, password).get();
            Cookie tokenCookie =
                    new Cookie(authenticationCookieConfig.getCookieName(), accessToken, "/", authenticationCookieConfig.getCookieDomain());

            NewCookie newCookie = new NewCookie(tokenCookie);

            return Response.seeOther(new URI(redirectSuccess)).cookie(newCookie).build();
        }
    }

    /*@GET
    @Path("/validate")
    @UnitOfWork
    public Response validate() {

    }*/
}
