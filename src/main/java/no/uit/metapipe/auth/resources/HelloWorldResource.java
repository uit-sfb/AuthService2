package no.uit.metapipe.auth.resources;

import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/hello")
public class HelloWorldResource {

    @GET
    @Timed
    public String sayHello() {
        return "hello world";
    }
}
