package no.uit.metapipe.auth.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

// Implements the "Standard Claims" as defined in "OpenID Connect Core 1.0 incorporating errata set 1"
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfo {
    @JsonProperty
    String sub;

    @JsonProperty
    String name;

    @JsonProperty("given_name")
    String givenName;

    @JsonProperty("family_name")
    String familyName;

    @JsonProperty
    String email;

    //BEGIN Non standard claims

    @JsonProperty
    String organization;
    @JsonProperty
    List<String> organizationAffiliations;

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public List<String> getOrganizationAffiliations() {
        return organizationAffiliations;
    }

    public void setOrganizationAffiliations(List<String> organizationAffiliations) {
        this.organizationAffiliations = organizationAffiliations;
    }
    //END Non standard claims

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
