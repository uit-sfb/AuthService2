package no.uit.metapipe.auth.utils;

import no.uit.metapipe.auth.core.Scope;

import java.util.HashSet;
import java.util.Set;

public class OAuthUtils {
    public static String buildScope(Set<String> scopes) {
        StringBuilder scope = new StringBuilder();
        for(String s : scopes) {
            if (scope.length() != 0) {
                scope.append(' ');
            }
            scope.append(s);
        }
        return scope.toString();
    }

    public static Set<String> encodeScope(Set<Scope> scopeSet) {
        Set<String> encodedScopes = new HashSet<>();
        for (Scope s : scopeSet) {
            encodedScopes.add(s.encodeString());
        }
        return encodedScopes;
    }

    public static Set<Scope> scopeFromStringSet(Set<String> requestedScopeStr) {
        Set<Scope> requestedScopes = new HashSet<>();
        for (String scopeStr : requestedScopeStr) {
            requestedScopes.add(Scope.fromString(scopeStr));
        }
        return requestedScopes;
    }
}
