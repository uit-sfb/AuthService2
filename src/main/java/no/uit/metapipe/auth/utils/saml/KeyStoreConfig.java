package no.uit.metapipe.auth.utils.saml;

import java.util.HashMap;
import java.util.Map;

public class KeyStoreConfig {
    private static final String DEFAULT_KEY = ""; // Removed default key and password from public repository
    private static final String DEFAULT_PASSWORD = ""; // Removed default key and password from public repository
    private static final String DEFAULT_LOCATION = "secret/samlKeystore.jks";

    String location;
    String password;
    Map<String, String> passwords;
    String defaultKey;

    public KeyStoreConfig() {
        if(location == null) {
            location = DEFAULT_LOCATION;
        }

        if(password == null && passwords == null && defaultKey == null) {
            password = DEFAULT_PASSWORD;
            passwords = new HashMap<>();
            passwords.put(DEFAULT_KEY, DEFAULT_PASSWORD);
            defaultKey = DEFAULT_KEY;
        }
    }

    public String getLocation() {
        return location;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Map<String, String> getPasswords() {
        return passwords;
    }

    public String getDefaultKey() {
        return defaultKey;
    }
}
