package no.uit.metapipe.auth.utils.saml;

import no.uit.metapipe.auth.config.SamlMetadataConfig;
import org.opensaml.saml2.metadata.provider.*;
import org.opensaml.util.resource.ClasspathResource;
import org.opensaml.xml.parse.ParserPool;
import org.springframework.context.ApplicationContext;
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate;

import java.io.File;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

public class SamlMetdataProviderFactory {
    private final ParserPool parserPool;

    public SamlMetdataProviderFactory(ParserPool parserPool) {
        this.parserPool = parserPool;
    }

    public static SamlMetdataProviderFactory fromApplicationContext(ApplicationContext context) {
        ParserPool parserPool = (ParserPool) context.getBean("parserPool");
        return new SamlMetdataProviderFactory(parserPool);
    }

    protected HTTPMetadataProvider createHttpMetadataProvider(SamlMetadataConfig config) {
        try {
            return new HTTPMetadataProvider(config.getLocation(), 15000);
        } catch (MetadataProviderException e) {
            throw new IllegalArgumentException("invalid configuration", e);
        }
    }

    protected ResourceBackedMetadataProvider createResourceBackedMetadataProvider(SamlMetadataConfig config) {
        try {
            URI location = URI.create(config.getLocation());
            return new ResourceBackedMetadataProvider(new Timer(), new ClasspathResource(location.getPath()));
        } catch (Exception e) {
            throw new IllegalArgumentException("invalid configuration", e);
        }
    }

    FilesystemMetadataProvider createFilesystemMetadataProvider(SamlMetadataConfig config) {
        URI filePathUri = URI.create(config.getLocation());
        String filePath;
        if(filePathUri.getScheme() == null) {
            filePath = config.getLocation();
        } else {
            filePath = filePathUri.getPath();
        }

        File file = new File(filePath);
        try {
            return new FilesystemMetadataProvider(file);
        } catch (MetadataProviderException e) {
            throw new IllegalArgumentException("invalid configuration", e);
        }
    }

    public MetadataProvider createFrom(SamlMetadataConfig config) {
        AbstractMetadataProvider metadataProvider;

        URI location = URI.create(config.getLocation());

        if (location.getScheme() == null) {
            metadataProvider = createFilesystemMetadataProvider(config);
        } else {
            switch (location.getScheme()) {
                case "http":
                    metadataProvider = createHttpMetadataProvider(config);
                    break;
                case "https":
                    metadataProvider = createHttpMetadataProvider(config);
                    break;
                case "resource":
                    metadataProvider = createResourceBackedMetadataProvider(config);
                    break;
                case "file":
                    metadataProvider = createFilesystemMetadataProvider(config);
                    break;
                default:
                    throw new IllegalArgumentException("Unrecognized scheme");
            }
        }

        metadataProvider.setParserPool(parserPool);

        if (config.getExtendedMetadata() != null) {
            return new ExtendedMetadataDelegate(metadataProvider, config.getExtendedMetadata());
        } else {
            return metadataProvider;
        }
    }

    public List<MetadataProvider> createFrom(List<SamlMetadataConfig> configList) {
        List<MetadataProvider> metadataProviders = new LinkedList<>();
        for(SamlMetadataConfig config : configList) {
            metadataProviders.add(createFrom(config));
        }
        return metadataProviders;
    }
}
