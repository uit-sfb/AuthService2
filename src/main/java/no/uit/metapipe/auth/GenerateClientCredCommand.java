package no.uit.metapipe.auth;

import io.dropwizard.Application;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import no.uit.metapipe.auth.config.MetapipeAuthServiceConfiguration;
import no.uit.metapipe.auth.config.ClientCredentialsConfig;
import no.uit.metapipe.auth.utils.SecretsGenerator;

public class GenerateClientCredCommand extends EnvironmentCommand<MetapipeAuthServiceConfiguration> {
    protected GenerateClientCredCommand(Application<MetapipeAuthServiceConfiguration> application) {
        super(application, "generate-client-credentials", "Generates client credentials that can be copied into the configuration file");
    }

    @Override
    protected void run(Environment environment, Namespace namespace, MetapipeAuthServiceConfiguration configuration) throws Exception {
        SecretsGenerator secretsGenerator = new SecretsGenerator();
        String clientId = "change-me" + secretsGenerator.createSecret(5);

        ClientCredentialsConfig config = ClientCredentialsConfig.createFromClientId(clientId, 10);

        System.out.println("- clientId: " + config.getClientId());
        System.out.println("  clientSecretHash: " + config.getClientSecretHash());
        System.out.println("  # clientSecret: " + config.getGeneratedSecret());
    }
}
