alter table tokens
    add column expiration int8;

update tokens
    set expiration = 0;

alter table tokens
    alter column expiration set not null;