create index tokenId_index on Tokens (tokenId);

create index userId_index on Users (username);

create index authorizationCode_index on AuthorizationCodes (authorizationCode);

create index externalUserId_index on ExternalUserId (identifier);

create index issuer_index on ExternalUserId (issuer);